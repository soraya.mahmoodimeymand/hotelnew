package com.example.hotel3.user.controller;

import com.sun.istack.NotNull;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class UserModelUpdate {
    @NotNull
    private Integer id;

    @NotBlank(message = "لطفا رمز عبور را وارد نمایید")
    private String password;


    @NotBlank(message = "لطفا نام کاربری را وارد کنید")
    private String username ;

    public static class UserModelOrder {


        @NotNull
        private Integer id;

        @NotBlank(message = "لطفا رمز عبور را وارد نمایید")
        private String password;



        @NotBlank(message = "لطفا نام کاربری را وارد کنید")
        private String username ;


        @NotBlank(message = "لطفا در خواست صورت حساب بدهید  ")
        private String categoryOrder ;

        Collection< UserModel> userModelList = new ArrayList<>();


    }
}
