package com.example.hotel3.user.controller;

public @interface NotBlank {
    String message();
}
