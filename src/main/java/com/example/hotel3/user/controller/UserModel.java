package com.example.hotel3.user.controller;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

    @Data
    public class UserModel {
        private Integer id;
        private String username;
        @JsonIgnore
        private String password;
        private String email;

         Collection<UserModelCallCancel>  UserModelCallCancelList=new ArrayList<>();

        Collection<UserModelCallrezerv> UserModelCallrezervlList=new ArrayList<>();

    }



