package com.example.hotel3.user.service;

import com.example.hotel3.user.controller.*;
import com.example.hotel3.user.controller.userModelCreat;

import java.util.List;


public interface UserService {
    UserModel create(userModelCreat userModelCreat);

    UserModel update(UserModelUpdate userModelUpdate);

    void delete(Integer id);

    List<UserModel> read();

    UserModel callrezerv(UserModelUpdate userModelUpdate);

    UserModel callCancel (UserModelUpdate userModelUpdate);

    List<UserModelCallrezerv> callrezerv();

    List<UserModelCallCancel> callcancel();

    List<UserModelCallorder> callorder();

    UserModel show(Integer id);




}
