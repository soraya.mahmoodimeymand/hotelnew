package com.example.hotel3.user.persistance;

import com.example.hotel3.user.persistance.User;
import com.example.hotel3.user.persistance.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;
import org.springframework.stereotype.Repository;

import javax.management.relation.Role;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository< CustomSQLErrorCodesTranslation, Integer> {
    @Query("select user from User user where user.id=:id")
    List<User> findAllByIDContaining(Integer id);


}
