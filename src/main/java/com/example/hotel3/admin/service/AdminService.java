package com.example.hotel3.admin.service;


import com.example.hotel3.admin.persistance.adminRepository;
import com.example.hotel3.category.persistance.category;
import com.example.hotel3.hotel.controller.HotelModel;
import com.example.hotel3.hotel.persistance.HotelAdminVerifiedEnum;
import com.example.hotel3.hotel.persistance.hotel;
import com.example.hotel3.hotel.persistance.hotelRepository;
import com.example.hotel3.hotel.service.hotelServicee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static javax.swing.UIManager.get;

@Service
@Transactional

public class AdminService implements AdminServicee {

    private hotelRepository hotelRepository;
    private categoryRepository categoryRepository;

    @Autowired
    public AdminService(hotelRepository hotelRepository,  categoryRepository  categoryRepository) {
        this.hotelRepository = photoRepository;
        this.categoryRepository = categoryRepository;


    }


    @Override
    public List<hotelModel> readByAdmin() {

        return hotelRepository.findAllByUserIdOOrderByIdDesc(userService.getId())
                .stream().map(hotelServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void categoryAdminVerfie(Integer id, HotelAdminVerifiedEnum status) {
        hotel hotel = (id);
        hotel.equals(id);
        hotelRepository.save(hotel);
    }

    @Override
    public List<HotelModel> indexByAdmin() {
        return hotelRepository.findByOrderByIdDesc()
                .stream().map(hotelServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public HotelModel showByAdmin(Integer id) {

        return convertEntityToModel(get(id)).toLIST();
    }


    @Override
    publicAdminModel create(AdminCreatModel AdminModelCreate) throws Exception {
        hotel newhotel = new Hotel();
        newhotel.setname(photoModelCreate.getname());
        newhotel.setPrice(photoModelCreate.getPrice());
        Set<category> categories = categoryService.getcategory(photoModelCreate.getCategory_id());
        checkCategoriesId(photoModelCreate.getCategory_id(), categories);
        newhotel.setCategory(categories);
        hotelRepository.save(newhotel);
        return convertEntityToModel(newhotel);
    }


    @Override
    public AdminModel update(AdminModelUpdate AdminModelUpdate) throws Exception {
        admin admin = admin(AdminModelUpdate.getId());
       Admin.setname(AdminModelUpdate.getname());
        Admin.setcodePersoneli(AdminModelUpdate.getcodePersoneli());
       userRepository.save(admin);
        return convertEntityToModel(admin);
    }

    @Override
    public void delete(Integer id) {
     admin admin = getadmin(id);
        adminRepository.delete(admin);
    }

    public static AdminModel convertToDto(admin  admin) {
        AdminModel adminModel =  new AdminModel();
       AdminModel.setId(admin.getId());
      AdminModel.setadminname(admin.getname());
       AdminModel.setcodePersoneli(admin.getcodePersoneli());


        return AdminModel;
    }



}
