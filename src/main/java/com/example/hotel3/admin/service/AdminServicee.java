package com.example.hotel3.admin.service;

import com.example.hotel3.hotel.persistance.HotelAdminVerifiedEnum;
import com.example.hotel3.user.controller.UserModel;
import com.example.hotel3.user.controller.UserModelUpdate;
import com.example.hotel3.user.controller.userModelCreat;

import java.util.List;

public interface AdminServicee {
    UserModel create(userModelCreat userModelCreate);


    UserModel update(UserModelUpdate userModelUpdate);

    void delete(Integer id);

    List<HotelModel> read();

    void categoryAdminVerfie ( Integer id, HotelAdminVerifiedEnum status);

}
