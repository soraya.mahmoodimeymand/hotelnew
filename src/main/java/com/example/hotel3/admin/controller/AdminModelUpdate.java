package com.example.hotel3.admin.controller;

import com.example.hotel3.user.controller.NotBlank;
import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class AdminModelUpdate {
    @NotNull
    private Integer id;

    @NotBlank(message = "کد پرسنلی را وارد کنید")
    private String  codePersoneli ;


}
