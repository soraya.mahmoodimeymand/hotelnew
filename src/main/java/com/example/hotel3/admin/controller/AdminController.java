package com.example.hotel3.admin.controller;

import com.example.hotel3.admin.service.AdminService;
import com.example.hotel3.category.persistance.category;
import com.example.hotel3.base.Response;
import com.example.hotel3.hotel.persistance.HotelAdminVerifiedEnum;
import com.example.hotel3.hotel.service.hotelService;
import com.example.hotel3.hotel.service.hotelServicee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin/")
public class AdminController {

    @Autowired
    private AdminService AdminService;
    public AdminController( AdminService adminService){
        this.AdminService=adminService;
    }

    @PostMapping( value = "creat/")
    public  AdminModel create( AdminModelCreate  adminModelCreate){
        return AdminService.creat ( adminModelCreate);
    }
    @GetMapping(value = {"/", ""})
    public List<AdminModel> read (){ return AdminService.read();}

    @PutMapping ( value = " update/")
    public  AdminModel update ( @RequestBody AdminModelUpdate adminModelUpdate ){
        return AdminService.update ( adminModelUpdate);
    }
    @DeleteMapping ( value= "delet/")
    public void delet ( Integer id) { AdminService.delet(id);}

    @PutMapping(value = {"/category/{id}", "/categoiry/{id}/"})
    public ResponseEntity<?> list(@PathVariable Integer id) throws Throwable {
        hotelServicee.categoryAdminVerfied(id, HotelAdminVerifiedEnum.SET);
        return ResponseEntity.ok(new Response(true, "", "هتل مورد نظر لیست شد", "", HttpStatus.OK));
    }

    @PutMapping(value = {"/reject/{id}", "/reject/{id}/"})
    public ResponseEntity<?> reject(@PathVariable Integer id) throws Throwable {
        hotelServicee.categoryAdminVerfied(id, HotelAdminVerifiedEnum.field);
        return ResponseEntity.ok(new Response(true, "", "هتل رد شد", "", HttpStatus.OK));
    }

}
