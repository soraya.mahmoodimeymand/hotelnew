package com.example.hotel3.admin.controller;

import com.example.hotel3.user.controller.NotBlank;
import lombok.Data;

@Data
public class AdminModelListingHotel {

    @NotBlank(message = " نوع هتل مورد نظر برای لیست شدن را وارد کنید")
    private String   category;
}
