package com.example.hotel3.admin.controller;

import com.example.hotel3.user.controller.NotBlank;
import lombok.Data;

@Data
public class AdminModelCreate {
    @NotBlank(message = " نام را وارد نمایید")
    private String name;

    @NotBlank(message = "کد پرسنلی  را وارد نمایید")
    private String codePersoneli;


}
