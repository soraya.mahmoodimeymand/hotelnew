package com.example.hotel3.admin.persistance;


import com.example.hotel3.base.RunConfiguration;
import com.example.hotel3.category.persistance.category;
import com.example.hotel3.hotel.persistance.hotel;
import com.example.hotel3.user.persistance.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@Table( name= "admin" , schema = RunConfiguration.DB)
public class admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "code_personeli", nullable = false)
    private String codePersoneli;


    @OneToMany(mappedBy = "admin", fetch = FetchType.LAZY)
    Collection<hotel> hotelList,

    @OneToMany(mappedBy = "admin", fetch = FetchType.LAZY)

    Collection<category> categoryList;

