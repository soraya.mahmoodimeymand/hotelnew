package com.example.hotel3.admin.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
    public interface adminRepository extends JpaRepository < admin,Integer> {
     admin findByCodePersoneli(String  codepersoneli);

}
