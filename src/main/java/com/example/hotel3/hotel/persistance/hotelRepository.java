
 package com.example.hotel3.hotel.persistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository

public interface hotelRepository  extends JpaRepository< hotel,Integer> {

    @Query("select hotel from hotel hotel where hotel.user.id=:userId")
    List<hotel> findAllByUserId(Integer userId);

    @Query("select hotel from hotel hotel  join hotel.category category where category.id=:categoryId")
    List<hotel> findAllByCategoryId(Integer categoryId);






}
