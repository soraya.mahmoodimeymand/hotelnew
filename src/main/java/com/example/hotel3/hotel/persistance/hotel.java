package com.example.hotel3.hotel.persistance;


import com.example.hotel3.base.RunConfiguration;
import com.example.hotel3.category.persistance.category;
import com.example.hotel3.order.persistance.order;
import com.example.hotel3.user.persistance.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.jdbc.support.CustomSQLErrorCodesTranslation;

import javax.persistence.*;
import java.util.Collection;

@Entity
    @Data
    @Table(name = "hotel",schema = RunConfiguration.DB)
    public class hotel {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)

        @Column(name = "id")
        private Integer id;

        @Column(name = "name",nullable = false)
        private String Name;

    @Column(name = "category",nullable = false)
    private Integer  Category;



    @Column(name = "admin_verified")
    @Enumerated(EnumType.ORDINAL)
    private HotelAdminVerifiedEnum adminVerified=HotelAdminVerifiedEnum.set;


    @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id",referencedColumnName = "id")
        private User user;


        @ManyToMany(fetch = FetchType.LAZY)
        @JoinTable(
                name = "categories_hotel",
                joinColumns = @JoinColumn(name = "hotel_id",referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "category_id",referencedColumnName = "id")
        )
        private Collection<category> categories;

        @OneToMany(mappedBy = "hotel",fetch = FetchType.LAZY)
        private Collection<order> ;

   Collection <user> userList;
    Collection<order >order;


    }


