package com.example.hotel3.base;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

public class Entity {


    @MappedSuperclass
    @EntityListeners(AuditingEntityListener.class)
    public class DateAudit {
        @CreationTimestamp
        @Column(name = "created_at",nullable = false,updatable = false)
        private Date createdAt;

        @UpdateTimestamp
        @Column(name = "updated_at")
        private Date updatedAt;
    }

}
