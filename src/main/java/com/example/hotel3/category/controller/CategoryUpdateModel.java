package com.example.hotel3.category.controller;

import com.example.hotel3.user.controller.NotBlank;
import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class CategoryUpdateModel {
    @NotNull
    private Integer id;

    @NotBlank(message = "لطفا نام را وارد کنید")
    private String name;

    private Integer parent_id;


}
