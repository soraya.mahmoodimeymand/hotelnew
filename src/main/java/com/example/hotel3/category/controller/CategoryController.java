package com.example.hotel3.category.controller;


import com.example.hotel3.admin.controller.AdminModel;
import com.example.hotel3.admin.controller.AdminModelCreate;
import com.example.hotel3.admin.controller.AdminModelUpdate;
import com.example.hotel3.category.service.CategoryService;
import com.example.hotel3.category.service.CategoryServicee;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")

public class CategoryController {
    private CategoryServicee categoryService;

    @Autowired
    public CategoryController(CategoryServicee categoryService) {
        this.categoryService = categoryService;
    }


    @PostMapping( value = "creat/")
    public CategoryModel create(CategoryCreatModel categoryCreatModel){
        return CategoryService.creat ( categoryCreatModel);
    }
    @GetMapping(value = {"/", ""})
    public List<CategoryModel> read (){ return CategoryService.read();}

    @PutMapping( value = " update/")
    public  CategoryModel update ( @RequestBody CategoryUpdateModel categoryUpdateModel ){
        return categoryService.update (categoryUpdateModel );
    }
    @DeleteMapping ( value= "delet/")
    public void delet ( Integer id) { CategoryService.delet(id);}


    @GetMapping(value = {"/",""})
    public ResponseEntity<?> read() throws Exception {
        List<CategoryModel> categoryModels = categoryService.read();
        return ResponseEntity.ok(new Response(true,categoryModels,"","", HttpStatus.OK));
    }
    @GetMapping(value = {"/tree","/tree/"})
    public ResponseEntity<?> tree() throws Exception {
        List<CategoryTreeChildMode> categories = categoryService.tree();
        return ResponseEntity.ok(new Response(true,categories,"","", HttpStatus.OK));
    }



}
