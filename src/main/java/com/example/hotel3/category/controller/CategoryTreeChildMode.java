package com.example.hotel3.category.controller;

import lombok.Data;

import java.util.List;
@Data
public class CategoryTreeChildMode {

    private Integer id;
    private String name;
    private List<CategoryTreeChildMode> child;

}
