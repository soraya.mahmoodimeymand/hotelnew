package com.example.hotel3.category.controller;

import lombok.Data;

import java.util.List;

@Data
public class CategoryCreatModel {
    private Integer id;

    private String name;

    private CategoryParentModel parent;

    private List<CategoryChildModel> child;

}
