package com.example.hotel3.category.controller;


import lombok.Data;

@Data
public class CategoryChildModel {
    private Integer id;

    private String name;

}
