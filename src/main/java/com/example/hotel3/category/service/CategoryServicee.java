package com.example.hotel3.category.service;

import com.example.hotel3.category.controller.*;
import com.example.hotel3.category.persistance.category;
import jdk.jfr.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

@Service
@Transactional

public class CategoryServicee implements CategoryService {

private categoryRepository categoryRepository;

    @Autowired
    public CategoryServicee(categoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Overrid
    public List<CategoryModel> read() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryServicee::convertEntityToModel)
                .collect(Collectors.toList());
    }


    public CategoryModel create(CategoryCreatModel category) {

        Category newCategory = new Category();
        newCategory.setName(category.getName());
        if (category.getParent_id() != null) {
            newCategory.setParent(getCategory(category.getParent_id()));
        }
        categoryRepository.save(newCategory);
        return convertEntityToModel(newCategory);
    }

    @Override
    public CategoryModel show(Integer id) {
        return convertEntityToModel(getCategory(id));
    }

    @Override
    public CategoryModel update(CategoryUpdateModel categoryModelUpdate) {
        Category category = getCategory(categoryModelUpdate.getId());
        category.setName(categoryModelUpdate.getName());
        if (categoryModelUpdate.getParent_id() != null) {
            category.setParent(getCategory(categoryModelUpdate.getParent_id()));
        }else{
            category.setParent(null);
        }
        categoryRepository.save(category);
        return convertEntityToModel(category);
    }

    @Override
    public void delete(Integer id) {
        category category = getCategory(id);
        categoryRepository.delete(category);
    }


    @Override
    public List<CategoryTreeChildMode> tree() {
        return categoryRepository.findAllByParentIsNull().stream()
                .map(CategoryServicee::convertEntityToTreeModel)
                .collect(Collectors.toList());
    }

    public static CategoryModel convertEntityToModel(Category category) {
        CategoryModel categoryModel =new CategoryModel();
        categoryModel.setId(category.getId());
        categoryModel.setName(category.getName());
        if (category.getParent()!=null){
            categoryModel.setParent(convertEntityToParentModel(category.getParent()));
        }
        if (category.getChildren()!=null){
            categoryModel.setChild(category.getChildren().stream().map(CategoryServicee::convertEntityToChildModel).collect(Collectors.toList()));
        }

        return categoryModel;
    }
    public static CategoryParentModel convertEntityToParentModel(Category category) {
        CategoryParentModel categoryParentModel =new CategoryParentModel();
        categoryParentModel.setId(category.getId());
        categoryParentModel.setName(category.getName());
        return categoryParentModel;
    }
    public static CategoryChildModel convertEntityToChildModel(Category category) {
        CategoryChildModel categoryChildModel =new CategoryChildModel();
        categoryChildModel.setId(category.getId());
        categoryChildModel.setName(category.getName());
        return categoryChildModel;
    }
    public static CategoryTreeChildMode convertEntityToTreeModel(Category category) {
        CategoryTreeChildMode categoryTreeChildModel =new CategoryTreeChildMode();
        categoryTreeChildModel.setId(category.getId());
        categoryTreeChildModel.setName(category.getName());
        categoryTreeChildModel.setChild(category.getChildren().stream().map(CategoryServicee::convertEntityToTreeModel).collect(Collectors.toList()));
        return categoryTreeChildModel;
    }
}

