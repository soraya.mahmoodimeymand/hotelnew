package com.example.hotel3.category.service;

import com.example.hotel3.category.controller.CategoryCreatModel;
import com.example.hotel3.category.controller.CategoryModel;
import com.example.hotel3.category.controller.CategoryTreeChildMode;
import com.example.hotel3.category.controller.CategoryUpdateModel;
import com.example.hotel3.category.persistance.category;

import java.util.List;
import java.util.Set;

public interface CategoryService {
    CategoryModel creat(CategoryCreatModel categoryModelCreate);


    CategoryModel show(Integer id);

    CategoryModel update(CategoryUpdateModel categoryModelUpdate);

    void delete(Integer id);

    List<CategoryModel> read();


    List<CategoryTreeChildMode> tree();

}
