package com.example.hotel3.category.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface categoryRepository  extends JpaRepository<category,Integer> {
    @Query("select category from Category category where category.parent is null ")
    List<category> findAllByParent();

}
