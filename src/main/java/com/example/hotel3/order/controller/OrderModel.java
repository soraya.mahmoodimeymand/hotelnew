package com.example.hotel3.order.controller;

import com.example.hotel3.order.persistance.OrderTypeEnum;
import com.example.hotel3.order.persistance.OrderaStatusEnum;
import com.example.hotel3.user.persistance.User;
import lombok.Data;

import java.util.Collection;

@Data
public class OrderModel {
    private Integer id;
    private Integer cost;
    private OrderaStatusEnum status;
    private Integer user_id;
    private String username;
    private OrderTypeEnum  status;


    Collection <User>  userList;


}
