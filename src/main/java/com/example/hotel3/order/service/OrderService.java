package com.example.hotel3.order.service;


import com.example.hotel3.hotel.controller.HotelModel;
import com.example.hotel3.hotel.persistance.hotel;
import com.example.hotel3.hotel.service.hotelServicee;
import com.example.hotel3.order.controller.OrderModel;
import com.example.hotel3.order.controller.OrderModelCreat;
import com.example.hotel3.order.persistance.OrderaStatusEnum;
import com.example.hotel3.order.persistance.order;
import com.example.hotel3.order.persistance.orderRepository;
import com.example.hotel3.user.controller.UserModel;
import com.example.hotel3.user.controller.userModelCreat;
import com.example.hotel3.user.persistance.User;
import com.example.hotel3.user.persistance.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional

public class OrderService implements OrderServicee {

    private orderRepository orderRepository;
    private UserRepository userRepository;

    @Autowired
    public OrderService(com.example.hotel3.order.persistance.orderRepository orderRepository, UserRepository UserRepository) {
        this.orderRepository = orderRepository;
        this.userRepository=userRepository;

    }
    @Override
    public  static OrderModel create(OrderModelCreat orderModelCreat) {
        order orde1 = new order();
        order1.setId(OrderModelCreat.getorderId());
    }


    @Override
    public OrderModel update(OrderModel orderModel) {
        Order order = getSingleOrder(orderModel.getId());
        order.setStatus(OrderaStatusEnum.pardakht);
        order.setAmount(OrderServicee.getAmount());
        orderRepository.save(order);
        return convertEntityToModel(order);
    }

    @Override
    public void changeStatus(Integer orderId, OrderStatusEnum status) {
       order order = getOrder(orderId);
        order.setStatus(status);
        orderRepository.save(order);
    }


    @Override
    public Boolean checkUserOrder() {
        if (orderRepository.findAllByUserId(userRepository.getId());
            return true;
        return false;
    }



    public static OrderModel convertEntityToModel(order order) {
        OrderModel orderModel = new OrderModel();
        orderModel.setId(order.getId());
        orderModel.setAmount(order.getAmount());
        orderModel.setStatus(order.getStatus());o
        orderModel.setUser_id(order.getUser().getId());
        orderModel.setUsername(order.getUser().getUsername());
        if (order.getUserList != null) {
            orderModel.getUserList()
                            .stream()
                            .map(orderServicee ::convertEntityToModel)
                            .collect(Collectors.toList());
        }
        return orderModel;

    }

    public static order convertModelToEntity(OrderModel orderModel) {
        order order = new order();
        order.setId(orderModel.getId());
        order.setAmount(orderModel.getAmount());
        order.setStatus(orderModel.getStatus());
        return order;
    }

}



