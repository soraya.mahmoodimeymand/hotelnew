package com.example.hotel3.order.service;

import com.example.hotel3.hotel.controller.HotelModel;
import com.example.hotel3.order.controller.OrderModel;

public interface OrderServicee {

    OrderModel create(UsrModel userId);

    OrderModel updat(UsrModel userId);


    void changeStatus(Integer orderId, OrderStatusEnum orderStatusEnum);

    Boolean checkUserOrder();

    Boolean checkUserOrder(Integer id);


}
