package com.example.hotel3.order.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface orderRepository extends JpaRepository<order,Integer> {
    @Query("select order from order order where order.user.id=:id and order.status=:status")
   order findOneByUserIdAndStatus(Integer id, OrderaStatusEnum status);

    @Query("select order from order order where order.user.id=:userId")
    List<order> findAllByUserId(Integer userId);



}
