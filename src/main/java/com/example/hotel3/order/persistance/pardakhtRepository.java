package com.example.hotel3.order.persistance;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
    public interface pardakhtRepository extends JpaRepository<pardakht,Integer> {

    pardakht findBybackCode (Integer backCode);
}
