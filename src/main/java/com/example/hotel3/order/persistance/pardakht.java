package com.example.hotel3.order.persistance;

import com.example.hotel3.base.RunConfiguration;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "pardakht",schema = RunConfiguration.DB)

public class pardakht {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Integer id;

    @Column(name = "Cost_rezerv")
    private Integer CostRezerv;
    @Column(name = "Cost_cancel")
    private Integer CostCancel;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PardakhtStatusEnam status;

    @Column(name = "back_code")
    private String backCode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id",referencedColumnName = "id")
    private order order;


}
